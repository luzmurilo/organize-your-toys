﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour
{
    public string boxTag;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(boxTag))
        {
            GameManager.SharedInstance.UpdateCount(1);
        }
        else
        {
            GameManager.SharedInstance.UpdateWarnText("Wrong Box!");
        }
    }
}
