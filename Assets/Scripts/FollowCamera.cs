using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Vector3 angleOffset;

    [SerializeField] Transform cameraTransform;

    // Start is called before the first frame update
    void Start()
    {
        if (cameraTransform == null)
            cameraTransform = GameObject.Find("Player Camera").transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localRotation = cameraTransform.localRotation;
        transform.Rotate(angleOffset); 
    }
}
