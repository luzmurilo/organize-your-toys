using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandleObjects : MonoBehaviour
{ 
    private ObjectsPool objectsPoolInstance;
    private GameObject objectAtHand;
    private bool isHolding;
    private bool waitingForObject;
    public float initialForce;
    public float maxForce;
    public float forceIncrease;
    [SerializeField] private float force;
    public Vector3 holdingPosition;

    [SerializeField] GameObject throwDirection;
    
    // Start is called before the first frame update
    void Start()
    {
        isHolding = false;
        waitingForObject = false;
        objectsPoolInstance = FindObjectOfType<ObjectsPool>();
        force = initialForce;
        
        if (throwDirection == null)
            throwDirection = GameObject.Find("Throw Direction");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(isHolding)
        {
            objectAtHand.transform.localPosition = holdingPosition;
        }
    }

    private void Update() {
        if (!isHolding && !waitingForObject)
        {
            StartCoroutine(AskForObject());
        }

        if (isHolding)
        {
            if (Input.GetMouseButton(0))
            {
                force += forceIncrease * Time.deltaTime;
                force = Mathf.Min(force, maxForce);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                ThrowObject(force);
                force = initialForce;
            }
        }

        
    }

    public void HoldObject(GameObject objectToHold)
    {
        objectToHold.transform.SetParent(transform);
        objectAtHand = objectToHold;
        isHolding = true;
        objectAtHand.transform.localRotation = Quaternion.Euler(0, 0, 0);
    }

    IEnumerator AskForObject()
    {
        waitingForObject = true;

        yield return new WaitForSeconds(2);

        GameObject tmp = objectsPoolInstance.GetNextObject();
        if (tmp != null)
        {
            HoldObject(tmp);
            waitingForObject = false;
        }
        else
            Debug.Log("No Object To Hold");
    }

    void ThrowObject(float throwForce)
    {
        Rigidbody objectRB = objectAtHand.GetComponent<Rigidbody>();

        objectAtHand.transform.SetParent(null);
        objectRB.useGravity = true;
        objectRB.AddForce(throwDirection.transform.forward * throwForce, ForceMode.Impulse);
        isHolding = false;
    }
}
